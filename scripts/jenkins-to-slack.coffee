module.exports = (robot) ->
  robot.router.get "/#{robot.name}/jenkins", (req, res) ->
    user = req.query.user
    branch = req.query.branch
    status = req.query.status
    channel = req.query.channel
    url = req.query.url

    messageText = switch status
        when "started"            then "Triggering a build of \"#{branch}\" on the Trybot. Check out: #{url}."
        when "success"            then "Your branch \"#{branch}\" landed successfully on master."
        when "merge-failure"      then "Attempting to merge \"#{branch}\" with master resulted in a merge conflict. Please resolve the conflict locally, and push again."
        when "push-failure"       then "Another commit beat your branch \"#{branch}\" onto master. The job will run again. Stand by..."
        when "test-failure"       then "Your build of \"#{branch}\" did not pass on the Trybot. Please check the Jenkins job: #{url}."
        when "connection-failure" then "The build slave couldn't access Github when it tried to run \"#{branch}\" on the Trybot. Is the slave ODLed? Please check the Jenkins job: #{url}."
        else "Something is wrong with hex's config >_<. Please check its integration with the Jenkins job."

    slackUser = {}
    slackUser.reply_to = user
    slackUser.room     = channel

    robot.send(slackUser, "@#{user}: #{messageText}")
    res.end "Said \"#{messageText}\""
