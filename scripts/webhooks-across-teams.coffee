TO_LAB_URL = 'https://hooks.slack.com/services/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'
FROM_ECOMM_TOKEN = 'GuKaPO9wET9APoZDxndM7Kfr'

TO_ECOM_URL = 'https://hooks.slack.com/services/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'
FROM_LAB_TOKEN = 'H152LhcrewbZ9xGKFHLsZPzf'

BOT_INDICATOR = '*'

module.exports = (robot) ->
	robot.router.post "/#{robot.name}/teamshare", (req, res) ->
		if (senderIsNotBot(req.body))
			console.log 'Sender is: ' + usernameOfSender(req.body)
			console.log 'Sender is not a bot'
			forwardMessage(req.body, robot)
		res.send 'OK'

senderIsNotBot = (requestBody) -> 
	console.log 'firstchar: ' + usernameOfSender(requestBody).charAt(0)
	console.log 'BOT_INDICATOR: ' + BOT_INDICATOR
	usernameOfSender(requestBody).charAt(0) != BOT_INDICATOR && usernameOfSender(requestBody) != 'slackbot'

usernameOfSender = (requestBody) ->
	requestBody.user_name

forwardMessage = (requestBody, robot) ->
	url = correctChannelUrl(requestBody.token)
	data = messageData(requestBody)
	robot.http(url).post(data) (err, res, body) ->
		console.log 'data: ' + data
		console.log 'url ' + url

correctChannelUrl = (token) ->
	switch token
		when FROM_ECOMM_TOKEN     then TO_LAB_URL
		when FROM_LAB_TOKEN       then TO_ECOM_URL

messageData = (requestBody) ->
	JSON.stringify({
		text: requestBody.text
		username: BOT_INDICATOR + usernameOfSender(requestBody)
	})